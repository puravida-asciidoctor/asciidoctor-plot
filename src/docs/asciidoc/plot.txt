[role='language-gb']
== Plot

Plot is a BlockProcessor to include Charts into your Asciidoctor documents. It's developed in a separate project so
you need to include the new dependency into your project

=== Gradle
Add PuraVida Bintray repository in your build.gradle

[#repositories_plot,source]
----
repositories {
    maven {
        url  "http://dl.bintray.com/puravida-software/repo"
    }
    // others
}
----

=== Version
Include the extension

[#dependencies_plot,source]
----
dependencies {
    asciidoctor "com.puravida.asciidoctor:asciidoctor-plot:{version}"
}
----

=== Examples
As soon you have installed it you can write _plot_ blocks as:

.example1
----
include::plot1.txt[]
----

include::plot1.txt[]

.example2
----
include::plot2.txt[]
----

include::plot2.txt[]
